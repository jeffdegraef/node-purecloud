var EventEmitter = require('events').EventEmitter;
var request = require('request');
var _ = require('underscore');
//var async = require('async');
module.exports = PureCloud;

function PureCloud(username, password, env, region, version)
{
    var self = this;
    var args = _.toArray(arguments);
    var callback = _.isFunction(_.last(args)) ? args.pop() : function(){};
    self.username = (args.length > 0) ? args.shift() : undefined;
    self.password = (args.length > 0) ? args.shift() : undefined;
    self.env      = (args.length > 0) ? args.shift() : "dca";
    self.region   = (args.length > 0) ? args.shift() : "us-east-1";
    self.version  = (args.length > 0) ? args.shift() : "v1";
    if(!_.isString(username) || !_.isString(password))
    {
        callback(new Error("Username and password are required parameters"));
        return;
    }
    self.root = (self.env === 'pca') ? "https://api.mypurecloud.com" : self.root = "https://public-api."+self.region+".inin"+self.env+".com";
    self.basePath = "/api/"+self.version+"/";
    self.loginData = {
        "userIdentifier": username,
        "password": password
    };

    if(callback) callback();

    EventEmitter.call(this);
}

PureCloud.super_ = EventEmitter;
PureCloud.prototype = Object.create(EventEmitter.prototype, {
    constructor: {
        value: PureCloud,
        enumerable: false
    }
});


PureCloud.funcDef = {
    'getAnalyticsAlertingAlerts':
    {
        method: 'GET',
        uriDef: 'analytics/alerting/alerts',
        args: [
            {
                name: 'pageNumber',
                required: false,
                type: 'qs'
            },
            {
                name: 'pageSize',
                required: false,
                type: 'qs'
            },
            {
                name: 'sortBy',
                required: false,
                type: 'qs'
            },
            {
                name: 'sortOrder',
                required: false,
                type: 'qs'
            }
        ]
    },
    'getAnalyticsAlertingAlertsUnread':
    {
        method: 'GET',
        uriDef: 'analytics/alerting/alerts/unread'
    },
    'createLogin':
    {
        method: 'POST',
        uriDef: 'login',
        args: [
            {
                name: 'body',
                required: true,
                type: 'body'
            }
        ]
    },
    'createSession':
    {
        method: 'POST',
        uriDef: 'auth/sessions',
        args: [
            {
                name: 'body',
                required: true,
                type: 'body'
            }
        ]
    },
    'getUsers': {
        method: 'GET',
        uriDef: 'users'
    },
    'getRoles': {
        method: 'GET',
        uriDef: 'authorization/roles'
    },
    'getUserRoles': {
        method: 'GET',
        uriDef: 'users/:userId/roles',
          args: [
            {
                name: 'userId',
                required: 'true'
            }
        ]
    },
    'getConfEdgeSoftwareVersions':
    {
        method: 'GET',
        uriDef: 'configuration/edges/<edgeid>/softwareversions',
        args: [
            {
                name: 'edgeid',
                required: true
            },
            {
                name: 'pageSize',
                required: false,
                type: 'qs'
            },
            {
                name: 'pageNumber',
                required: false,
                type: 'qs'
            }
        ]
    },
    'getConfEdges':
    {
        method: 'GET',
        uriDef: 'configuration/edges'
    },
    'getHealthCheck':
    {
        method: 'GET',
        uriDef: 'health/check'
    }
};


PureCloud.prototype.baseFunction = function(args)
{
    var self = this;
    var cb = args.pop();
    if(!_.isFunction(cb))
    {
        console.log("No callback function defined");
        return;
    }
    var name;
    if(args.length > 0) name = args.shift();
    var func = PureCloud.funcDef[name];
    if(func === undefined)
    {
        cb("No function named "+name);
        return;
    }
    var uriDef = func.uriDef;
    var err = "";
    var options = {};
    options.method = func.method;
    options.headers = {};
    if(_.isArray(func.args))
    {
        _.each(func.args, function(arg)
        {
            var val;
            if(args.length > 0) val = args.shift(); else val = arg.def;
            if(arg.required && val === undefined)
                err += arg.name+" is required ";
            else
            {
                if(arg.type === undefined)
                {
                    if(arg.required)
                        uriDef = uriDef.replace('<'+arg.name+'>', val);
                    else if(val !== undefined)
                    {
                        uriDef = uriDef.replace('['+arg.name+']', val);
                    }
                }
                else if(arg.type === 'qs')
                {
                    if(options.qs === undefined) options.qs = {};
                    options.qs[arg.name] = val;
                }
                else if(arg.type === 'body')
                {
                    options.body = val;
                    options.json = true;
                }
            }
        });
        uriDef = uriDef.replace(/\[[^\]]*\]/g, "");
        uriDef = uriDef.replace(/(\/)\/+/g, "$1");
    }
    if(uriDef === 'health/check')
        options.uri = self.root+'/'+uriDef;
    else
        options.uri = self.root+self.basePath+uriDef;
//    console.log(options);
    var sendRequest = function()
    {
        var errMsg;
        if(_.isString(self.apiToken))
        {
            options.headers["ININ-Auth-Api"] = self.apiToken;
            options.headers["X-OrgSpan-Auth-Key"] = self.apiToken;
            options.headers["X-OrgBook-Auth-Key"] = self.apiToken;
        }
        if(_.isString(self.sessionToken))
        {
            options.headers["ININ-Session"] = self.sessionToken;
            options.headers.Authorization = "IPC " + self.sessionToken;
        }
        request(options, function(error, response, body)
        {
//            console.log(error);
//            console.log(response);
//            console.log(body);
//            console.log(response.statusCode);
            if(_.isObject(response) &&
               _.isObject(response.headers) &&
               _.isString(response.headers['content-type']))
            {
                var contentType = _.first(response.headers['content-type'].toLowerCase().split(';'));
                switch(contentType)
                {
                    case 'application/json':
                        body = _.isString(body) ? JSON.parse(body) : body;
                    break;
                    default:
                    break;
                }
                if(Math.floor(response.statusCode/100) !== 2)
                {
                    if(_.isObject(body) && _.isString(body.message))
                    {
                        errMsg = new Error(body.message);
                        if(/Invalid login credentials/i.test(body.message)) {
                            delete self.apiToken;
                            delete self.sessionToken;
                        }
                    }
                    else if(_.isString(body))
                    {
                        errMsg = new Error(body);
                    }
                }
            }
            if(_.isObject(response) &&
               _.isObject(response.headers) &&
               _.isArray(response.headers['set-cookie']))
            {
                var ininAuthCookie = _.find(response.headers['set-cookie'], function(c)
                {
                    return (c.search(/^ININ-Auth-Api=.*$/) !== -1);
                });
                if(_.isString(ininAuthCookie))
                {
                    var parsedCookie = ininAuthCookie.match(/^ININ-Auth-Api=([^;]*);.*$/);
                    if(_.isArray(parsedCookie) && _.isString(parsedCookie[1]))
                        self.apiToken = parsedCookie[1];
                }
            }
            cb(errMsg || error, body);
        });
    };
    if((self.apiToken === undefined || self.sessionToken === undefined) && uriDef !== 'login' && uriDef !== 'auth/sessions')
    {
        self.login(function(err, data)
        {
            if(err)
                cb(err, data);
            else
            {
                sendRequest();
            }
        });
    }
    else
    {
        sendRequest();
    }
};

PureCloud.prototype.login = function(callback)
{
    var self = this;
    self.createLogin(self.loginData, function(err, data)
    {
        if(err)
            callback(err);
        else
        {
            self.createSession(self.loginData, function(err, data)
            {
                if(err)
                    callback(err);
                else
                {
                    if(_.isObject(data) && _.isString(data.id))
                    {
                        self.sessionToken = data.id;
                        self.sessionData = data;
                    }
                    callback(err, data);
                }
            });
        }
    });
};

_.each(PureCloud.funcDef, function(func, name)
{
    if(PureCloud.prototype[name] === undefined)
    {
        PureCloud.prototype[name] = function()
        {
            var self = this;
            var args = _.toArray(arguments);
            args.unshift(name);
            self.baseFunction(args);
        };
    }
});

